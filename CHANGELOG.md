# Changelog

## Version 0.1

* Basic functionality.
* `WriteTerminal`, `ReadTerminal`, `Echo`, `SortString`, and `Collect` stages.
* Tests.
* Snap-ins.