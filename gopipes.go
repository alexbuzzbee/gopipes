// Package gopipes implements a concurrent object pipeline mechanism like in MS PowerShell using goroutines and channels.
package gopipes

import (
	"errors"
	"fmt"
	"strings"
)

// A Pipe is an arbitrary object pipe.
type Pipe chan interface{}

// StageFunc is a function that can be a pipeline stage.
type StageFunc func(input, output, err Pipe, params []string)

// A Stage is an element that takes in a stream of objects and outputs another such stream.
type Stage struct {
	params []string
	code   StageFunc
}

// A Pipeline is a chain of Stages to be connected together with Pipes.
type Pipeline []Stage

var stages = map[string]StageFunc{}

// Launch starts goroutines for all the stages of a pipeline and connects them with pipes.
func Launch(p Pipeline) (in, out, err Pipe) {
	lineIn := make(Pipe)
	lineErr := make(Pipe)
	current := lineIn
	for _, stage := range p {
		var out = make(Pipe)
		if stage.code == nil {
			panic(fmt.Errorf("invalid stage %v", stage))
		}
		go stage.code(current, out, lineErr, stage.params)
		current = out
	}
	return lineIn, current, lineErr
}

// Create builds a pipeline out of a string.
func Create(spec string) (line Pipeline, err error) {
	defer func() {
		if p := recover(); p != nil {
			err = p.(error)
		}
	}()
	specs := strings.Split(spec, "|")
	line = Pipeline(make([]Stage, len(specs)))
	for i, stage := range specs {
		params := strings.Split(strings.Trim(stage, " "), " ")
		if stages[params[0]] == nil {
			panic(fmt.Errorf("no such command %v", params[0]))
		}
		line[i] = Stage{params, stages[params[0]]}
	}
	return
}

// Run is a wrapper around Create and Launch.
func Run(spec string) (in, out, errStrm Pipe, err error) {
	line, err := Create(spec)
	if err != nil {
		return nil, nil, nil, err
	}
	in, out, errStrm = Launch(line)
	return
}

// NewStage registers a new type of stage. It is not concurrency-safe.
func NewStage(name string, impl StageFunc) (err error) {
	defer func() {
		if p := recover(); p != nil {
			err = p.(error)
		}
	}()
	if stages[name] != nil {
		panic(errors.New("cannot register a stage type that already exists"))
	}
	stages[name] = impl
	return nil
}
