package gopipes

import (
	"fmt"
	"testing"
)

func TestEcho(t *testing.T) {
	_, out, _, err := Run("Echo first second third | Collect")
	if err != nil {
		t.Error(err.Error())
	}
	s := (<-out).([]interface{})
	for i, val := range s {
		v := val.(string)
		switch i {
		case 0:
			if v != "first" {
				t.Errorf("Wrong first value %v.", v)
			}
		case 1:
			if v != "second" {
				t.Errorf("Wrong second value %v.", v)
			}
		case 2:
			if v != "third" {
				t.Errorf("Wrong third value %v.", v)
			}
		}
	}
}

func ExampleWriteTerminal() {
	_, out, _, err := Run("Echo hello | WriteTerminal")
	if err != nil {
		panic(err)
	}
	for range out {
	}
	// Output:
	// hello
}

func TestSortString(t *testing.T) {
	_, out, _, err := Run("Echo b c a | SortString")
	if err != nil {
		t.Error(err.Error())
	}
	var last string
	for item := range out {
		if item.(string) < last {
			t.Errorf("Wrong order: %v came before %v.", last, item)
		}
	}
}

func ExampleForEach() {
	f := func(obj interface{}) interface{} {
		fmt.Println(obj)
		return nil
	}
	line, err := Create("Echo first second third | ForEach")
	if err != nil {
		panic(err)
	}
	line[1].code = ForEach(f)
	_, out, _ := Launch(line)
	for range out {
	}
	// Output:
	// first
	// second
	// third
}

func TestNewStage(t *testing.T) {
	err := NewStage("Stop", func(input, output, err Pipe, params []string) { close(output) })
	if err != nil {
		t.Error("Failed to register a stage type.")
	}
	err = NewStage("Stop", func(input, output, err Pipe, params []string) { close(output) })
	if err == nil {
		t.Error("Able to duplicate a stage type.")
	}
}

func TestRun(t *testing.T) {
	_, _, _, err := Run("Nope | Nada")
	if err == nil {
		t.Error("Able to use nonexistant stages.")
	}
}
