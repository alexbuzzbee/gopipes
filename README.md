# Go package gopipes

[![pipeline status](https://gitlab.com/alexbuzzbee/gopipes/badges/master/pipeline.svg)](https://gitlab.com/alexbuzzbee/gopipes/commits/master)

Package gopipes implements a concurrent object pipeline mechanism like in MS PowerShell using goroutines and channels.
