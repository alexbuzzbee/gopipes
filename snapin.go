package gopipes

import (
	"os"
	"plugin"
)

// LoadSnapin loads a snap-in containing gopipes stages.
func LoadSnapin(name string) (err error) {
	_, err = plugin.Open(os.Getenv("GPSNAPINS") + "/" + name + ".so")
	return
}
