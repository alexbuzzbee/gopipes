package gopipes

import (
	"fmt"
	"reflect"
	"sort"
)

// PipeToSlice (which was stolen from StackOverflow) reads all data from ch (which must be a chan), returning a
// slice of the data. If ch is a 'T chan' then the return value is of type
// []T inside the returned interface.
// A typical call would be sl := ChanToSlice(ch).([]int)
// It is exported to make writing stalling stages easier.
func PipeToSlice(ch interface{}) interface{} {
	chv := reflect.ValueOf(ch)
	slv := reflect.MakeSlice(reflect.SliceOf(reflect.TypeOf(ch).Elem()), 0, 0)
	for {
		v, ok := chv.Recv()
		if !ok {
			return slv.Interface()
		}
		slv = reflect.Append(slv, v)
	}
}

// WriteTerminal writes its input to the host's standard output.
func WriteTerminal(input, output, err Pipe, params []string) {
	defer func() {
		if p := recover(); p != nil { // If we panic (input not printable probably), write it to the error pipe.
			err <- p
		}
	}()
	defer close(output)
	for item := range input {
		fmt.Printf("%v\n", item)
	}
}

// ReadTerminal reads from the host's standard input line by line and outputs them as strings.
func ReadTerminal(input, output, err Pipe, params []string) {
	defer func() {
		if p := recover(); p != nil { // If we panic (I/O error probably), write it to the error pipe.
			err <- p
		}
	}()
	defer close(output)
	line := " " // So we don't instantly stop reading.
	for line != "" {
		fmt.Scanln(&line)
		output <- line
	}
}

// Echo outputs each of its parameters.
func Echo(input, output, err Pipe, params []string) {
	defer func() {
		if p := recover(); p != nil { // If we panic (I/O error probably), write it to the error pipe.
			err <- p
		}
	}()
	defer close(output)
	for _, arg := range params[1:] {
		output <- arg
	}
}

// SortString sorts strings from the input pipe. Because it sorts, it must wait for all input to arrive before it can send any output, so it stalls the pipeline.
func SortString(input, output, err Pipe, params []string) {
	defer func() {
		if p := recover(); p != nil { // If we panic (input not strings probably), write it to the error pipe.
			err <- p
		}
	}()
	defer close(output)
	strs := PipeToSlice(input).([]string)
	sort.Strings(strs)
	for _, str := range strs {
		output <- str
	}
}

// Collect collects input objects into a slice and outputs the slice.
func Collect(input, output, err Pipe, params []string) {
	defer func() {
		if p := recover(); p != nil { // If we panic, write it to the error pipe.
			err <- p
		}
	}()
	defer close(output)
	output <- PipeToSlice(input)
}

// ForEach creates a wrapper around a function that calls it for each item moving through the pipeline.
func ForEach(f func(obj interface{}) interface{}) StageFunc {
	return func(input, output, err Pipe, params []string) {
		defer func() {
			if p := recover(); p != nil { // If we panic, write it to the error pipe.
				err <- p
			}
		}()
		defer close(output)
		for item := range input {
			output <- f(item)
		}
	}
}

func init() {
	NewStage("WriteTerminal", WriteTerminal)
	NewStage("ReadTerminal", ReadTerminal)
	NewStage("Echo", Echo)
	NewStage("SortString", SortString)
	NewStage("Collect", Collect)
	NewStage("ForEach", ForEach(func(obj interface{}) interface{} { return nil })) // This is a kludge to allow using ForEach in pipeline specs.
}
